jQuery.ajax = (function(_ajax){

	var protocol = location.protocol,
		hostname = location.hostname,
		exRegex = RegExp(protocol + '//' + hostname);

	function isExternal(url) {
		return !exRegex.test(url) && /:\/\//.test(url);
	}

	return function(o) {

		var url = o.url;

		if (isExternal(url))
			o.url = 'proxy.php?url=' + encodeURIComponent(url) + '&dataType=' + o.dataType + '&method=' + o.type;
		
		return _ajax.apply(this, arguments);
	};
})(jQuery.ajax);