var HiperClient = function() {
	
	var debug = false;

	var catalogUri;
	var monitorUri;
	
	var monitorId = 0;
	var graphTimeout;
	
	var qinputs = {};
	var queryUrl;
	
	var username;
	var password;
	
	var authentic;

	var links = {
		'': getMonitorsList,
		'monitors': getMonitorsList,
		'monitors/(\\d+)': getFeedsList,
		'monitors/(\\d+)/feeds': getFeedsList,
		'monitors/(\\d+)/feeds/(\\d+)': getFeedDetails,
		'monitors/(\\d+)/entities': getEntitiesList,
		'monitors/(\\d+)/entities/(\\d+)': getFeedsList,
		'catalog/(\\d+)/feeds': getCatalogueFeeds,
		'catalog/(\\d+)/entities/(\\d+)/feeds': getCatalogueFeeds,
		'catalog/(\\d+)/entities': getCatalogueEntities
	};
	
	function insertLoginForm(fun) {
		var html = '';
		html += '<table id="loginFormTable">';
		html += '<tr>';
		html += '<td><p>Login:</p></td><td><input type="text" id="nickInput" value="igor"/></td>';
		html += '</tr><tr>';
		html += '<td><p>Password:</p></td><td><input type="password" id="passInput" value="ala123"/></td>'
		html += '</tr><tr>';
		html += '<td colspan="2"><input type="button" id="loginButt" value="Log in"/></td>';
		html += '</tr>';
		html += '</table>';
	
		insertContent(html);
		$("#loginButt").click(fun);
	}
	
	function init(uri, deb) {
		if (deb != undefined)
			debug = deb;

		registerLoader();
		initPopup();
		deactiveNav();
		
		insertLoginForm(function(){
			username = $("#nickInput").val();
			password = $("#passInput").val();
			authentic = username + ':' + password + '@';
			
			catalogUri = urlOptimizer(uri);
			$.history.init(initDeepLinks);
		});
	}

	function optimizeQueryParam(param){
		var result = param;
		result = result.replace('_', ' ');
		return result[0].toUpperCase() + result.substring(1);
	}
	
	function qFormCreate(query) {
	
		queryUrl = query;
		
		var html = '';
		html += '<input type="button" id="qFormShowButt" value="Show search form"/>';
		html += '<div id="qFormDiv">';
		html += '<br/><br/><br/><br/><br/><table id="queryTableForm">';

		if (query != undefined) {
			var vars = query.split('?')[1].split('&');
			qinputs = new Array();
			
			for(var i = 0; i<vars.length; i++){
				
				var qid = 'qinput' + i;
				var label = optimizeQueryParam(vars[i].split('=')[0]);
				
				html += '<tr><td>' + label + '</td><td><input type="text" id=' + qid + ' ></input></td></tr>';
				qinputs.push(qid);
			}
			html += '</table>'
			html += '<input type="button" id="qFormSubmitButt" value="Search"/>'
			html += '</div>';
		}
		
		return html;
	}
	
	//function qFormSubmit() {
	//	var realQuery = queryUrl;
	//	for(k in qinputs){
	//		realQuery = realQuery.replace(/{}/, document.getElementById(qinputs[k]).value);
	//		//alert(document.getElementById(qinputs[k]).value);
	//	}
	//	alert(realQuery);
	//}
		
	function qFormActions(fun) {
		$('#qFormDiv').hide();
		$('#qFormShowButt').toggle(function(){
			$('#qFormDiv').show('fold', {}, 250);
		}, function() {
			$('#qFormDiv').hide('fold', {}, 250);
			
		});
		
		$('#qFormSubmitButt').click(function() {
			var realQuery = queryUrl;
			for(k in qinputs){
				realQuery = realQuery.replace(/{}/, document.getElementById(qinputs[k]).value);
				//alert(document.getElementById(qinputs[k]).value);
			}
			//alert(realQuery);
			fun(realQuery);
		});
	}
	
	function getMonitorsList() {
		get(catalogUri + '/monitors', function(xml) {
			
			var monitors = xml.find('monitor');
			var html = '';
			
			html += '<h1>Catalog</h1><ul>';
			html += '<li><a class="catalogAnch" href="#catalog/0/feeds">' + catalogUri + '</a></li>';
			html += '</ul><br/>';
			
			html += '<h1>Monitors</h1><ul>';

			monitors.each(function(i, e) {
				var el = $(e);
				var id = el.attr('id');

				html += '<li><a class="monitorAnch" href="#monitors/' + id + '/feeds" data-monitor-url="' + el.find('monitor_url').text() + '" data-monitor-id="' + id + '">' + el.find('monitor_url').text() + '</a></li>';
			});

			html += '</ul>';

			insertContent(html);
			
			$('#main section a.catalogAnch').click(function() {
				monitorUri = catalogUri;
				monitorId = undefined;
			});

			$('#main section a.monitorAnch').click(function() {
				monitorUri = urlOptimizer($(this).data('monitor-url'));
				monitorId = $(this).data('monitor-id');
			});
		});

		deactiveNav();
	}
	
	function getCatalogueFeeds(entityId, query) {
		
		var url;
		if(query == undefined)
			url = catalogUri + (entityId != undefined ? '/entities/' + entityId + '/feeds' : '/feeds');
		else 
			url = query;

		get(url, function(xml) {
			
			var html = '<h1>Catalogued feeds';

			if (entityId != undefined) 
				html += ' from  entity \'' + xml.find('entity').attr('href') + '\'';

			// | ID | typ | metryka | entity
			html += ':</h1><table><thead><tr><th>Id</th><th>Type</th><th>Metric (unit)</th><th>Entity</th></tr></thead><tbody>';
			
			var pattern = new RegExp("^([\\w:\\.\\d\\/]*)\/feeds\/(\\d+)(\\/?)$", 'i');
		
			xml.find('feeds > feed').each(function(i, e) {
			
				var $e = $(e);
				
				var feedId = $e.attr('id');
				var type = $e.attr('type');
				var metric = $e.find('metric');
				var entity = (entityId != undefined ? entityId : $e.find('entity').attr('id'));
				
				var match = pattern.exec($(e).attr('href'));
				
				var monUrl = match[1];
				var monId = $e.attr('monitor_id');
				var feedInMonId = match[2];
					
				var idAnch = '<a href="#monitors/' + monId + '/feeds/' + feedInMonId + '" data-mon-url="' + monUrl + '" data-mon-id="' + monId + '" class="feedFromCatAnch">' + feedId + '</a>';
				var entAnch = '<a href="#catalog/0/entities/' + entity + '/feeds">' + entity + '</a>';
				
				if(type == 'SIMPLE') {
					html += '<tr><td>' + idAnch + '</td><td>SIMPLE</td><td>' + metric.find('name').text() + ' (' + metric.find('unit').text() + ')</td><td>' + entAnch + '</td></tr>';
				}
				else {
					html += '<tr><td>' + idAnch + '</td><td>' + type + '</td><td></td><td>' + entAnch + '</td></tr>';
					
					$e.find('feed').each(function() {
						match = pattern.exec($(this).attr('href'));
						
						monUrl = match[1];
						feedInMonId = match[2];
						
						metric = $(this).find('metric');
						
						html += '<tr><td>' + feedId + '/' + feedInMonId + '</td><td>SIMPLE</td><td>' + metric.find('name').text() + ' (' + metric.find('unit').text() + ')</td><td>' + entAnch + '</td></tr>';
					});
				}
			});
		
			html += '</tbody></table>';
			
			var query = xml.find('feeds query');
			if(query != undefined) {
				log('feeds query: ' + query.attr('href'));
				html += qFormCreate(query.attr('href'));
			}
			
			insertContent(html);
			
			$('a.feedFromCatAnch').click(function() {
				log('feedFromCatAnch anchor click callback.');
				monitorUri = $(this).data('mon-url');
				monitorId = $(this).data('mon-id');
			});
			
			if(query != undefined)
				qFormActions(function (q) {
					getCatalogueFeeds(undefined, q);
				});
		});

		activateNav('feeds', '#catalog/0');
	}
	
	function getCatalogueEntities(query) {
	  
		var url = (query != undefined ? query : catalogUri + '/entities');

		get(url, function(xml) {

			var html = '<h1>Entities</h1><table><thead><tr><th>Id</th><th>Name</th></tr></thead><tbody>';

			xml.find('entities entity').each(function(i, e) {
			
				var $e = $(e);
				var href = $e.attr('href');
				
				var pattern = new RegExp("^([\\w:\\.\\d\\/]*)\/entities\/(\\d+)(\\/?)$", 'i');
				var match = pattern.exec($(e).attr('href'));
				
				var id = match[2];
				
				var name = $e.find('name');
				name = (name != undefined ? name.text() : id);

				html += '<tr><td>' + id + '</td><td><a href="#catalog/0/entities/' + id + '/feeds" >' + name + '</a></td></tr>';
			});

			html += '</tbody></table>';
			
			var query = xml.find('entities query');
			if(query != undefined) {
				log('entities query: ' + query.attr('href'));
				html += qFormCreate(query.attr('href'));
			}
		
			insertContent(html);
			
			if(query != undefined)
				qFormActions(function(q) {
					getCatalogueEntities(q);
				});
		});

		activateNav('entities', '#catalog/0');
	}
	
	function getFeedsList(entityId, addParams) {

		if (monitorUri == undefined) {
			log('Have to wait for monitor url');
			setTimeout(function() {getFeedsList(entityId);}, 100);
			return;
		}

		var url = (entityId != undefined ? '/entities/' + entityId + '/feeds' : '/feeds');

		get(monitorUri + url, function(xml) {

			var entity = xml.find('entity > name').text();
			var html = '<h1>Feeds';

			if (entityId != undefined)
				html += ' from ' + entity + ' entity';

			html += '</h1><table><thead><tr><th>Id</th><th>Type</th><th>Metric (unit)</th><th>Entity</th><th>Actions</th></tr></thead><tbody>';
		
			xml.find('feeds feed').each(function(i, e) {
			
				var $e = $(e);
				var id = $e.attr('id');
				var actions = '<a href="#monitors/' + monitorId + '/feeds/' + id + '" title="Show feed details" class="search"></a>';
				var metric = $e.find('metric');

				if (entityId == undefined)
					entity = $e.find('entity name').text();

				if ($e.attr('type') == 'SIMPLE')
					actions += '<a href="#" class="add" title="Create new AVG feed" data-id="' + id + '"></a> <input type="checkbox" name="feeds[]" value="' + id + '" />';
				else
					actions += '<a href="#" class="delete" title="Delete this feed" data-id="' + id + '"></a>';

				html += '<tr><td>' + id + '</td><td>' + $e.attr('type') + '</td><td>' + metric.find('name').text() + ' (' + metric.find('unit').text() + ')</td><td>' + entity + '</td><td>' + actions + '</td></tr>';
			});
		
			html += '</tbody></table><input type="button" id="createList" value="Create list" />';
			
			insertContent(html);
			
			feedsActions(entityId);
		});

		activateNav('feeds',  '#monitors/' + monitorId);;
	}

	function getEntitiesList() {

		if (monitorUri == undefined) {
			log('Have to wait for monitor url');
			setTimeout(function() {getEntitiesList();}, 100);
			return;
		}

		get(monitorUri + '/entities', function(xml) {

			var html = '<h1>Entities</h1><table><thead><tr><th>Id</th><th>Name</th><th>Actions</th></tr></thead><tbody>';

			xml.find('entities entity').each(function(i, e) {
			
				var $e = $(e);
				var id = $e.attr('id');

				html += '<tr><td>' + id + '</td><td>' + $e.find('name').text() + '</td><td><a href="#monitors/' + monitorId + '/entities/' + id + '" title="Show feeds from this entity" class="search"></a></td></tr>';
			});

			html += '</tbody></table>';
		
			insertContent(html);
		});

		activateNav('entities', '#monitors/' + monitorId);
	}

	function getFeedDetails(feedId) {

		if (monitorUri == undefined) {
			log('Have to wait for monitor url');
			setTimeout(function() {getFeedDetails(feedId);}, 100);
			return;
		}

		get(monitorUri + '/feeds/' + feedId, function(xml) {
			
			var feed = xml.find('feed');
			var metric = feed.find('metric');
			var unit = metric.find('unit').text();
			var type = feed.attr('type');

			html = '<p><b>Id:</b> ' + feed.attr('id') + '</p>';
			html += '<p><b>Type:</b> ' + type + '</p>';
			html += '<p><b>Entity:</b> ' + feed.find('entity name').text() + '</p><p>&nbsp;</p>';
			html += '<p><b>Metric (unit):</b> ' + metric.find('name').text() + ' (' + unit + ')</p>';
			html += '<p><b>Available measurements:</b> ' + feed.find('available_measurements').text() + '</p><p>&nbsp;</p>';

			if (type == 'AVG') {

				var measurement = feed.find('measurement');

				html += '<p><b>Average period:</b> ' + feed.find('avg_period').text() + '</p>';
				html += '<p><b>Measurement value:</b> ' + measurement.find('value').text() + unit + '</p>';
				html += '<p><b>Measurement time:</b> ' + measurement.find('time').text() + '</p>';
			} else if (type == 'LIST') {

				html += '<ul>'

				feed.find('feeds feed').each(function(i, e) {
					var id = $(this).attr('id');
					html += '<li>Feed <a href="#monitors/' + monitorId + '/feeds/' + id + '">#' + id + '</a></li>';
				});

				html += '</ul>';
			} else {
				html += '<div id="graph"></div>';
			}

			insertPopup(html, 'Feed details');

			if (type == 'SIMPLE')
				getGraph(feedId);
		});
	}

	function getGraph(feedId) {

		if (graphTimeout != undefined) {
			clearTimeout(graphTimeout);
			graphTimeout = undefined;
		}

		if ($('#graph').length) {
			get(monitorUri + '/feeds/' + feedId + '/measurements', function(xml) {
				printGraph(feedId, xml);
			});
		}
	}

	function printGraph(feedId, xml) {
		
		if ($('#graph').length) {
			
			var html = '<table><thead><tr><th></th><th style="color: #00AEEF;">metric</th></tr></thead><tbody>';

			xml.find('measurements measurement').each(function(i, e) {

				var e = $(this);
				var dateObject = new Date(parseInt(e.find('date').text() + '000'));
				var date = datePrint(dateObject.getHours()) + ':' + datePrint(dateObject.getMinutes()) + ':' + datePrint(dateObject.getSeconds());
				html += '<tr><th>' + date + '</th><td>' + e.find('value').text() + '</td></tr>';
			});

			html += '</tbody></table>';

			$('#graph').html(html);

			var colors = [], custom = [];

			$("#graph table thead th:not(:first)").each(function() {
				colors.push($(this).css("color"));
			});
			$("#graph table tbody th").each(function() {
				custom.push($(this).html());
			});
        
			$('#graph table').graphTable({
				series: 'columns',
				position: 'replace',
				width: '100%',
				height: '250px',
				colors: colors,
				xaxisTransform: function(label) {
					
					var i = 0;

					while ((i < custom.length) && (label != custom[i])) {
						i++;
					}

					return i;
				}
			}, {
				xaxis: {
					tickSize: 1,
					tickFormatter: function(v, a) {
						return custom[v];
					}
				},
				yaxis: {
					max: null,
					autoscaleMargin: 0.02
				}
			});

			graphTimeout = setTimeout(function() {getGraph(feedId);}, 5000);
		}
	}

	function datePrint(val) {

		if (val < 10)
			val = '0' + val;
		
		return val;
	}

	function feedsActions(entityId) {

		$('#createList').click(function(e) {

			e.preventDefault();

			if ($('input[name=\'feeds[]\']:checked').length == 0) {
				alert('Please select feeds from which you want to create list.');
				return;
			}

			var data = 'type=LIST&access=PUBLIC';

			$('input[name=\'feeds[]\']:checked').each(function(i, e) {
				data += '&feeds[]=' + $(this).val();
			});

			post(monitorUri + '/feeds/', function(xml) {
				getFeedsList(entityId);
			}, data);
		});

		$('div#main section table a.add').click(function(e) {

			e.preventDefault();

			do {

				var period = prompt("Please enter period measurement for new feed (number)");

			} while (period != null && (period == '' || isNaN(period)));

			if (!period)
				return;

			var data = 'type=AVG&access=PUBLIC&id=' + $(this).data('id') + '&period=' + period;

			post(monitorUri + '/feeds/', function(xml) {
				getFeedsList(entityId);
			}, data);
		});

		$('div#main section table a.delete').click(function(e) {

			e.preventDefault();

			if (confirm('Are you sure?')) {
				deleteRequest(monitorUri + '/feeds/' + $(this).data('id') + '/', function(xml) {
					getFeedsList(entityId);
				});
			}
		});
	}

	function getMonitorUri(id) {

		log('Need to obtain monitor url');
		monitorUri = undefined;

		get(catalogUri + '/monitors/' + id, function(xml) {
			monitorUri = urlOptimizer(xml.find('monitor_url').text());
			monitorId = id;
		});
	}

	function initDeepLinks(hash) {

		var fun = undefined;
		var arg = undefined;

		for (var el in links) {

			var pattern = new RegExp("^" + el + "$", 'i');
			
			if (pattern.test(hash)) {

				fun = links[el];
				var match = pattern.exec(hash);

				if (match[1] != undefined && (monitorUri == undefined || match[1] != monitorId)) {
					getMonitorUri(match[1]);
				}

				if (match[2] != undefined)
					arg = match[2];

				break;
			}
		}

		if (fun != undefined)
			fun(arg);
		else
			error404();
	}

	function urlOptimizer(url) {

		var pattern = /\/$/i;

		if (pattern.test(url))
			url = url.substring(0, url.length-1);
		
		return url;
	}

	function get(url, fun) {
		request(url, fun, 'GET');
	}

	function deleteRequest(url, fun) {
		request(url, fun, 'DELETE');
	}

	function post(url, fun, data) {
		request(url, fun, 'POST', data);
	}

	function request(url, fun, method, data) {
		
		log('[' + method + '] "' + url + '" started');
		
		if(url.substring(0,3) == "www")
			url = "http://" + authentic + url.substring(4);
		else
			url = url.substring(0,7) + authentic + url.substring(7);

		if (data == undefined)
			data = '';

		$.ajax({
			url: url,
			type: method,
			dataType: 'xml',
			//username: username,
			//password: password,
			data: data,
			success: function(data) {

				if (data == '') {

					log('[' + method + '] "' + url + '" succeeded, but empty response');

					error404();
					return;
				}
				
				log('[' + method + '] "' + url + '" succeeded');

				fun($(data));
			},
			error: function(jqXHR, textStatus, errorThrown) {

				log('[' + method + '] "' + url + '" failed', errorThrown);

				if (jqXHR.status == 404)
					error404();
				else
					unknownError();
			}
		});
	}
	
	function initPopup() {
		$('#popup').dialog({
			height: 600,
			width: 1000,
			modal: true,
			draggable: false,
			resizable: false,
			autoOpen: false,
			close: function(event, ui) {
				location.hash = 'monitors/' + monitorId + '/feeds';
			}
        });
	}

	function registerLoader() {
		$('#ajaxLoading').ajaxStart(function() {
			$(this).show();
		}).ajaxStop(function() {
			$(this).hide();
		});
	}

	function deactiveNav() {
		$('header nav').hide();
	}

	function activateNav(element, hrefPrefix) {

		$('header nav').show();	

		$('body > header nav ul li.active').removeClass('active');
		$('body > header nav ul li a.' + element).parent().addClass('active');

		$('body > header nav ul li a.feeds').attr('href', hrefPrefix + '/feeds');
		$('body > header nav ul li a.entities').attr('href', hrefPrefix + '/entities');
		
	}

	function error404() {
		insertContent('<h1>Error 404</h1><p>There is no such page.</p>');
		$('body > header nav ul li.active').removeClass('active');
	}

	function unknownError() {
		insertContent('<h1>Unknown error</h1><p>Unknown error has occurred.</p>');
		$('body > header nav ul li.active').removeClass('active');
	}
	
	function insertPopup(html, title) {

		$('#popup').html(html);
		$('#popup').parent().find('.ui-dialog-title').html(title);
		$('#popup').dialog('open');
	}

	function insertContent(html) {

		$('#popup').dialog('close');
		$('#popup').html('');
		$('#main section').html(html + '<div class="clearfix"></div>');

		if ($('#main section table a').length)
			$('#main section table a').tooltip({
				track: true
			});
	}

	function log(text, additional) {

		if (debug) { 

			console.log(text);

			if (additional != undefined) {
				console.log(additional);
				console.log(additional.message);
			}
		}
	}

	return {
		init: init
	}
};

$(document).ready(function() {
	var client = HiperClient();
	client.init('http://catalog.codewatch.pl', true);
});
