<?php

	if ($_GET['dataType'] == 'xml')
		header('Content-Type: text/xml');

	$ch = curl_init(urldecode($_GET['url']));

	if (isset($_GET['method']) && $_GET['method'] == 'POST') {

		$fields = '';

		foreach ($_POST as $key => $value) {

			$fields .= $key.'=';

			if (is_array($value)) {
				foreach ($value as $val)
					$fields .= $val.',';
				$fields = substr($fields, 0, -1).'&';
			} else
				$fields .= $value.'&';
		}

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	} elseif (isset($_GET['method']) && $_GET['method'] == 'DELETE')
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

	//curl_setopt($ch, CURLOPT_USERPWD, 'user:password'); 
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$output = curl_exec($ch);       
	curl_close($ch);

	echo $output;
