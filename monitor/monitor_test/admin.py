from django.contrib import admin
from monitor_test.models import Feed, Measurement, Entity, Metric, Operations


admin.site.register(Feed)
admin.site.register(Operations)
admin.site.register(Metric)
admin.site.register(Entity)
admin.site.register(Measurement)