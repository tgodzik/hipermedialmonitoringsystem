from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from monitor_test.models import *
from django.shortcuts import render
import xml.etree.ElementTree as ET
from django.views.decorators.csrf import csrf_exempt
from functools import wraps
from django.template import Context, Template
from PyCharm import settings
import requests
import logging

logger = logging.getLogger('monitor_test.views')

def update_monitor():
    file=open('../templates/register_monitor.xml','r')
    template= file.read()
    t = Template(template)

    list=[]
    for e in Entity.objects.all():
        tmp=[]
        for f in Feed.objects.filter(entity=e).all():
            if f.type != 'L':
                c=Measurement.objects.filter(feed=f).all().count()
            else:
                c=1
            tmp.append((f,c))
        list.append((e,tmp))
    c = Context({"monitor": settings.MONITOR,"list":list})
    #print t.render(c)
    print settings.monitor_url
    logger.info("pre put")
    r = requests.put(settings.monitor_url,data=t.render(c), headers={'content-type':'text/xml','accept':'text/xml'}, auth=('igor', 'ala123') )
    logger.info("post put")
    cnt = '<dupa>' + r.content + '</dupa>'
    settings.monitor_url = ET.fromstring(cnt).find('monitoring').find('monitors').find('monitor').attrib['href']

def http_basic_auth(func):
    @wraps(func)
    def _decorator(request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.META.has_key('HTTP_AUTHORIZATION'):
            authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
            if authmeth.lower() == 'basic':
                auth = auth.strip().decode('base64')
                username, password = auth.split(':', 1)
                if username in ['igor','pawel'] and password == 'ala123':
                    return func(request, True, *args, **kwargs)
                else:
                    return func(request, False, *args, **kwargs)
            else:
                return func(request, False, *args, **kwargs)
        else:
            return func(request, False, *args, **kwargs)
    return _decorator

def renderError(request, code, msg):
    return render(request, 'errorview.xml', {"msg": msg, "code": code, "title": "Something gone terribly wrong!"}, content_type="text/xml", status=code)

def renderAccessDenied(request):
    return renderError(request, 401, 'AccessDenied')

def entitieslist(request):
    if request.method == 'GET':
        list = Entity.objects.all()
        return render(request, 'entitieslist.xml', {"list": list}, content_type="text/xml")

def entities(request, id):
    if request.method == 'GET':
        entity = Entity.objects.get(id=id)
        feeds = Feed.objects.filter(entity=entity)
        print entity.name
        return render(request, 'entity.xml', {'entity': entity, 'feeds': feeds}, content_type="text/xml")


def entitiesfeeds(request, id):
    if request.method == 'GET':
        entity = Entity.objects.get(id=id)
        feeds = Feed.objects.filter(entity=entity)
        return render(request, 'entitiesfeeds.xml', {'entity': entity, 'feeds': feeds}, content_type="text/xml")

def profile(request):
    if request.method == 'GET':
        return render(request, 'profile.txt',{}, content_type="text")

@csrf_exempt
@http_basic_auth
def feedslist(request, authorized):
    if request.method == 'GET':
        list = Feed.objects.all()
        return render(request, 'feedslist.xml', {"list": list}, content_type="text/xml")
    if request.method == 'POST':
        op=request.POST['type']
        if op == "AVG":
            f = AverageFeed()
            f.maxAverage = request.POST['period']
            f.type = 'A'
            f.basicFeed = Feed.objects.get(id=request.POST['id'])
            f.metric = f.basicFeed.metric
            f.access = request.POST['access']
            f.entity = f.basicFeed.entity
            f.save()
            return render(request, 'feed.xml', {"feed": f}, content_type="text/xml")
        elif op == "LIST":
            f = Feed()
            f.type = 'L'
            #to chyba nie ma znaczenia?
            feeds = request.POST['feeds'].split(',')
            basicFeed = Feed.objects.get(id=feeds[0])
            f.metric = basicFeed.metric
            f.access = request.POST['access']
            f.entity = basicFeed.entity
            f.save()
            lf = ListFeed()
            lf.feed = f
            for iden in feeds:
                lf.childFeed = Feed.objects.get(id=iden)
                lf.pk=None
                lf.save()
            return render(request, 'feed.xml', {"feed": lf}, content_type="text/xml")

@csrf_exempt
@http_basic_auth
def feeds(request, authorized, id):
    if request.method == 'GET':
        feed = Feed.objects.get(id=id)
        if feed.access == 0 and authorized == False:
            return renderAccessDenied(request)
        if feed.type == 'A':
            feed = AverageFeed.o
        count=Measurement.objects.all().filter(feed=feed).all().count()
        return render(request, 'feed.xml', {"feed": feed,"count":count}, content_type="text/xml")
    if request.method == 'DELETE':
        feed=Feed.objects.get(id=id)
        if feed.type!='S':
            feed.delete()
        return render(request, 'empty.xml', {}, content_type="text/xml")

@http_basic_auth
def feedsmeasurements(request, authorized, id):
    if request.method == 'GET':
        feed = Feed.objects.get(id=id)
        if feed.access == 0 and authorized == False:
            return renderAccessDenied(request)
        list = Measurement.objects.filter(feed=feed).order_by('time')
        return render(request, 'measurements.xml', {'list': list, "feed": feed}, content_type="text/xml")

@http_basic_auth
def feedsmeasurementsno(request, authorized, id, m_no):
    if request.method == 'GET':
        feed = Feed.objects.get(id=id)
        if feed.access == 0 and authorized == False:
            return renderAccessDenied(request)
        measurement = Measurement.objects.filter(feed=feed).get(inverted_id=m_no)
        return render(request, 'feedsmeasurementsno.xml', {'msmt': measurement, "feed": feed}, content_type="text/xml")

def operationslist(request):
    if request.method == 'GET':
        list = Operations.objects.all()
        return render(request, 'operationslist.xml', {"list": list}, content_type="text/xml")

def operations(request, name):
    if request.method == 'GET':
        #operation = Operations.objects.get(id=id)
        if name=="create_feed_avg":
            return render(request, 'create_feed_avg.xml', {"operation": id}, content_type="text/xml")
        else :
            return render(request, 'create_feed_list.xml', {"operation": id}, content_type="text/xml")

def metricslist(request):
    if request.method == 'GET':
        list = Metric.objects.all()
        return render(request, 'metricslist.xml', {"list": list}, content_type="text/xml")

def metrics(request, id):
    if request.method == 'GET':
        metric = Metric.objects.get(id=id)
        return render(request, 'metrics.xml', {"metric": metric}, content_type="text/xml")

#test
def createavgfeed(request,id):
    f = AverageFeed()
    f.basicFeed = Feed.objects.get(pk=1)
    f.maxAverage = id
    f.type = 'A'
    f.metric = f.basicFeed.metric
    f.access = f.basicFeed.access
    f.entity = f.basicFeed.entity
    f.save()
    return HttpResponse("AVG")

#test
def createlistfeed(request):
    f = Feed()
    basicFeed = Feed.objects.get(pk=1)
    f.type = 'L'
    f.metric = basicFeed.metric
    f.access = basicFeed.access
    f.entity = basicFeed.entity
    f.save()

    lf = ListFeed()
    lf.feed = f
    lf.childFeed = Feed.objects.get(pk=1)
    lf.pk=None
    lf.save()
    lf.childFeed = Feed.objects.get(pk=2)
    lf.pk=None
    lf.save()
    lf.childFeed = Feed.objects.get(pk=3)
    lf.pk=None
    lf.save()

    return HttpResponse("LIST")