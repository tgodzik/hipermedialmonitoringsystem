from rest_framework import serializers
from monitor_test import models
from monitor_test.models import *

class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measurement
        fields = ('id', 'value', 'name')

class MetricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metric
        fields = ('id', 'unit', 'name')

class BaseFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseFeed
        fields = ('id', 'metric', 'measurement')

class EntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ('id', 'name', 'feed')
