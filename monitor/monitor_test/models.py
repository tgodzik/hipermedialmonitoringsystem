import django.contrib.gis.measure
from django.db import models

from django.contrib.auth.models import User

class Entity(models.Model):
    name = models.CharField(max_length=200)

class Operations(models.Model):
    name = models.CharField(max_length=20)

class Metric(models.Model):
    unit = models.CharField(max_length=20)  #% || st.C
    name = models.CharField(max_length=200) #cpu-usage

class Feed(models.Model):
    TYPES = (('S','SIMPLE'),('A','AVG'),('L','LIST'))
    ACCESS = ((0,'PRIVATE'),(1,'PUBLIC'))
    metric = models.ForeignKey(Metric)
    access = models.BooleanField()
    entity = models.ForeignKey(Entity)
    type=models.CharField(max_length=2, choices=TYPES)

    def getAccessStr(self):
        return self.ACCESS[self.access][1]
    def getTypeStr(self):
        MAP = {'S':0, 'A':1, 'L':2}
        return self.TYPES[MAP[self.type]][1]
    def measurements(self):
        if type=='S':
            return Measurement.objects.filter(feed=self)
        else:
            ret = []
            for lf in ListFeed.objects.filter(feed=self).all():
                ret = ret + Measurement.objects.filter(feed=lf.childFeed).all()
            return ret

    def subFeeds(self):
        return ListFeed.objects.filter(feed=self).all()

#jak takich bedzie wiele to stworza liste dude
class ListFeed(models.Model):
    #id feeda ktory agreguje
    feed = models.ForeignKey(Feed,related_name='parentFeed')
    #id feeda ktory jest na liscie
    childFeed = models.ForeignKey(Feed,related_name='childFeed')

class AverageFeed(Feed):
    #ilosc pomiarow z ktorych liczymy srednia
    maxAverage = models.IntegerField()
    #feed, ktorego srednia liczymy
    basicFeed = models.ForeignKey(Feed,related_name='basicFeed')

    def average(self):
        msmts = Measurement.objects.filter(feed=self.basicFeed).all()[:self.maxAverage]
        sum = 0
        for m in msmts:
            sum = sum + m.value
        return sum / self.maxAverage

class Measurement(models.Model):
    feed = models.ForeignKey(Feed)
    value = models.IntegerField()
    inverted_id=models.IntegerField()
    time = models.BigIntegerField()
