
import socket
import threading
import time
import os
import httplib2
import urllib
from django.core.management import setup_environ
from PyCharm import settings
from django.template import Context, Template
import requests

import xml.etree.ElementTree as ET
setup_environ(settings)

from monitor_test.models import *

#TODO - czy to dziala?
monitor_url=""
# Our thread class:
class ClientThread ( threading.Thread ):

    lock=threading.Lock()

    # Override Thread's __init__ method to accept the parameters needed:
    def __init__ ( self, channel, details):
        self.channel = channel

        self.details = details

        threading.Thread.__init__ ( self )

    def run ( self ):

        register=self.channel.recv (512)


        list=register.split(":")

        #REGISTER:ENTITY:entity_name:METRIC:metric_name:UNIT:unit_name:\n
        if (list[0]=="REGISTER"):

            ClientThread.lock.acquire(True)

            create=True;

            for e in Entity.objects.all():

                if e.name==list[2]:

                    create=False
                    self.entity=e

            if create:

                self.entity=Entity()

                self.entity.name=list[2]

                self.entity.save()

            #tworzymy nowa metryke jesli potrzeba
            create=True
            nlist = Metric.objects.all()

            for m in nlist:
                if(m.name==list[4] and m.unit==list[6]):
                    create=False
                    self.metric=m

            if create:
                self.metric=Metric()
                self.metric.name=list[4]
                self.metric.unit=list[6]
                self.metric.save()
                #metryk nie ma potrzeby kasowac

            self.feed=Feed()
            self.feed.entity=self.entity
            self.feed.access=1
            self.feed.metric=self.metric
            self.feed.type='S'
            self.feed.save()

            self.measurement=Measurement()
            self.measurement.feed=self.feed
            update_monitor()
            ClientThread.lock.release()
        receive=1
        while(receive):
            self.measurement.time=time.time()

            receive=self.channel.recv (512)

            if not receive: break

            list=receive.split(":") #data update
            self.measurement.value=int(list[1])
            self.measurement.time=time.time()
            self.measurement.pk=None
            self.measurement.inverted_id=0
            for m in Measurement.objects.filter(feed=self.feed).all().order_by('inverted_id')[10:]:
                m.delete()
            for m in Measurement.objects.filter(feed=self.feed):
                m.inverted_id+=1
                m.save()
            self.measurement.save()
        self.close()

    def close(self):
        self.channel.close()
#  nie zadziala hah      self.measurement.delete()
#        self.feed.delete()
        print 'Closed connection:', self.details [ 0 ]

class ServerThread(threading.Thread ):

    def run(self):

        # Set up the server:
        self.run=True
        self.clients=[]
        self.server = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )

        self.server.bind ( ( '127.0.0.1', 3336 ) )

        self.server.listen ( 5 )

        while self.run:

            channel, details = self.server.accept()

            #print "Connection received"

            tmp=ClientThread ( channel, details)
            tmp.start()
            self.clients.append(tmp)

    def close(self):
        for c in self.clients:
            if c.isAlive():
                c.close()
        for e in Entity.objects.all():
            e.delete()
        self.server.close()

class UpdateThread(threading.Thread ):

    def run(self):
        while self.run:
            import time
            time.sleep(5)
            update_monitor()


def register_monitor():
#    print os.getcwd()
    file=open('./templates/register_monitor.xml','r')
    template= file.read()
    t = Template(template)

    list=[]
    for e in Entity.objects.all():
        tmp=[]
        for f in Feed.objects.filter(entity=e).all():
            c=Measurement.objects.filter(feed=f).all().count()
            tmp.append((f,c))
        list.append((e,tmp))
    c = Context({"monitor": settings.MONITOR,"list":list})
    #TODO czy beda zwracac id w content
#    print t.render(c)
    r = requests.post(settings.CATALOGUE+"/monitors",data=t.render(c), headers={'content-type':'text/xml','accept':'text/xml'}, auth=('igor', 'ala123') )
    cnt = '<dupa>' + r.content + '</dupa>'
    print cnt
    settings.monitor_url = ET.fromstring(cnt).find('monitoring').find('monitors').find('monitor').attrib['href']

def update_monitor():
    file=open('./templates/register_monitor.xml','r')
    template= file.read()
    t = Template(template)

    list=[]
    for e in Entity.objects.all():
        tmp=[]
        for f in Feed.objects.filter(entity=e).all():
            print f
            if f.type != 'L':
                c=Measurement.objects.filter(feed=f).all().count()
            else:
                c=1
            tmp.append((f,c))
        list.append((e,tmp))
    c = Context({"monitor": settings.MONITOR,"list":list})
    #print t.render(c)
#    print settings.monitor_url
    r = requests.put(settings.monitor_url,data=t.render(c), headers={'content-type':'text/xml','accept':'text/xml'}, auth=('igor', 'ala123') )
    cnt = '<dupa>' + r.content + '</dupa>'
    settings.monitor_url = ET.fromstring(cnt).find('monitoring').find('monitors').find('monitor').attrib['href']


def run():

    #przy uruchamianiu kasujemy wszystko
    for e in Entity.objects.all():
        e.delete()
    for e in Metric.objects.all():
        e.delete()
    register_monitor()
    updateThread=UpdateThread()
    updateThread.daemon=True
    updateThread.start()
    listenerServer=ServerThread()
    listenerServer.daemon=True
    listenerServer.start()

if __name__=='__main__' :
    #przy uruchamianiu kasujemy wszystko
    for e in Entity.objects.all():
        e.delete()
    for e in Metric.objects.all():
        e.delete()
    register_monitor()


    updateThread=UpdateThread()
    updateThread.daemon=True
    updateThread.start()
    # Set up the server:
    run=True
    clients=[]
    server = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )

    server.bind ( ( '', 3336 ) )

    server.listen ( 5 )

    while run:
        channel, details = server.accept()
        tmp=ClientThread ( channel, details)
        tmp.start()
        clients.append(tmp)
