#!/usr/bin/env python
import os
import sys
import listener

if __name__ == "__main__":
    listener.run()
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "PyCharm.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)