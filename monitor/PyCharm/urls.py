from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('monitor_test.views',
                    url(r'^entities/$', 'entitieslist'),
                    url(r'^entities/(?P<id>\d+)/$', 'entities'),
                    url(r'^entities/(?P<id>\d+)/feeds/$', 'entitiesfeeds'),

                    url(r'^profile/$', 'profile'),

                    url(r'^feeds/$', 'feedslist'),
                    url(r'^feeds/(?P<id>\d+)/$', 'feeds'),
                    url(r'^feeds/(?P<id>\d+)/measurements/$', 'feedsmeasurements'),
                    url(r'^feeds/(?P<id>\d+)/measurements/(?P<m_no>\d+)/$', 'feedsmeasurementsno'),

                    url(r'^metrics/$', 'metricslist'),
                    url(r'^metrics/(?P<id>\d+)/$', 'metrics'),

                    url(r'^test/cavgf/(?P<id>\d+)/$', 'createavgfeed'),
                    url(r'^test/cavgl$', 'createlistfeed'),
)

urlpatterns += patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
