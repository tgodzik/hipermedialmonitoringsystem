namespace :db do
	task :populate => :environment do
		require 'rubygems'
		require 'populator'
		MonitorNode.populate 20 do |monitorNode|
			monitorNode.monitor_url = Populator.words(1..3).titleize
		end
	end
end