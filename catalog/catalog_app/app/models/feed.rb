class Feed < ActiveRecord::Base

	attr_accessible :avg_period, :available_measurements, :measurement_url, :feed_type, :monitor_feed_url, :username, :access

	has_many :feed_parts, foreign_key: "custom_feed_id", class_name: "FeedPart"
	has_many :simple_feeds, through: :feed_parts

	belongs_to :entity
	belongs_to :metric

	attr_accessor :non_persistent_subfeeds_urls

	def monitor_node
		entity.monitor_node
	end

	PUBLIC = 1
	SIMPLE = 0

	@@FEED_TYPE_TO_INT = {"SIMPLE" => 0, "AVG" => 1, "LIST" => 2}
	@@FEED_TYPE_TO_STRING = ["SIMPLE", "AVG", "LIST"]

	@@ACCESS_TO_INT = {"PRIVATE" => 0, "PUBLIC" => 1}
	@@ACCESS_TO_STRING = ["PRIVATE", "PUBLIC"]

	'' '@param data: {"href"=>"www.monitor.com/feeds/11/",
            "type"=>"SIMPLE",
            "metric"=>{"name"=>"cpu_usage", "unit"=>"%"},
            "available_measurements"=>"2",
            "measurement"=>{"href"=>"www.monitor.com/feeds/1/measurement"}}' ''

	def self.new_from_hash(data)
		Feed.new.tap do |f|
			f.monitor_feed_url= data[:href]
			f.feed_type = get_model_feed_type(data[:type])
			f.measurement_url=data[:measurements][:href]
			f.available_measurements=data[:available_measurements].to_i
			f.non_persistent_subfeeds_urls = []
			if f.feed_type == 0
				handle_simple(data, f)
			elsif f.feed_type == 1
				handle_avg(data, f)
			elsif f.feed_type == 2
				handle_list(data, f)
			end
		end
	end


	def self.handle_simple(data, feed)
		feed.metric = Metric.get_or_create(data[:metric])
	end

	def self.handle_avg(data, feed)
		feed.access=get_model_access(data[:access])
		feed.username=data[:username]

		feed.avg_period=data[:avg_period].to_i
	end

	def self.handle_list(data, feed)
		feed.access=get_model_access(data[:access])
		feed.username=data[:username]

		#hash_subfeeds = data[:feed]
		#if (hash_subfeeds.class != Array)
		#	hash_subfeeds = [data[:feed]]
		#end
		#
		#hash_subfeeds.each do |hash_f|
		#	subfeed_url = hash_f[:href]
		#	feed.non_persistent_subfeeds_urls << subfeed_url
		#end
	end


	def self.get_model_access(type)
		@@ACCESS_TO_INT[type.upcase]
	end

	def self.get_string_access(type)
		@@ACCESS_TO_STRING[type]
	end

	def self.get_model_feed_type(type)
		@@FEED_TYPE_TO_INT[type.upcase]
	end

	def self.get_string_feed_type(type)
		@@FEED_TYPE_TO_STRING[type]
	end


	def populate_with_subfeeds
		p Feed.get_string_feed_type(feed_type)

		@non_persistent_subfeeds_urls.each do |url|
			simple_feed = Feed.where(:monitor_feed_url => url)
			simple_feeds<<simple_feed
		end
	end

	def type
		Feed.get_string_feed_type(feed_type)
	end
end
