class FeedPart < ActiveRecord::Base
	belongs_to :simple_feed, class_name: "Feed"
	belongs_to :custom_feed, class_name: "Feed"
end
