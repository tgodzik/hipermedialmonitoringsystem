class Metric < ActiveRecord::Base

	attr_accessible :name, :unit

	has_many :feeds

	'''@param data: {"name"=>"cpu_usage", "unit"=>"%"}'''
	def  self.get_or_create(data)
		metric = Metric.find_or_create_by_name_and_unit(data[:name],data[:unit])
		metric
	end
end
