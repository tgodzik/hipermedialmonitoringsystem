class MonitorNode < ActiveRecord::Base

	attr_accessible :monitor_url

	has_many :entities, dependent: :destroy
	has_many :feeds, through: :entities, dependent: :destroy

	'' '@param data: {"monitor_url"=>"www.monitorx.com",
   	"entities"=>
   	 {"entity"=> ... }}  ' ''

	def self.new_from_hash(data)
		MonitorNode.new.tap do |m|
			m.monitor_url = data[:monitor_url]
			if data[:entities]!=nil then
				m.new_entities_from_hash(data)
			end
		end
	end

	def update_subfeeds(feed_list)
		p feed_list
		p "-----"
		feed_list.each do |f|
			p f.non_persistent_subfeeds_urls;
			f.populate_with_subfeeds()
		end
		save
	end


	def new_entities_from_hash(data)
		hash_entities = data[:entities][:entity]

		#when there is only one entity it is not in list. we have to fix it
		if (hash_entities.class != Array)
			hash_entities = [data[:entities][:entity]]
		end

		#create entity model from each entity hash representation and add it to monitor
		hash_entities.each do |hash_e|
			if hash_e != nil
				entity = Entity.new_from_hash(hash_e)
				self.entities << entity
			end
		end
	end

	def save_monitor
		MonitorNode.transaction do
			feed_list = []
			entities.each do |e|
				feed_list += e.feeds
			end
			save
			update_subfeeds(feed_list)
		end
	end

	def update_monitor(update_hash)
		new_monitor = MonitorNode.new_from_hash(update_hash[:data])
		MonitorNode.transaction do
			#destroy all associated entities and feeds
			entities.each do |entity|
				entity.destroy
			end

			#associate new data
			monitor_url = new_monitor.monitor_url
			entities=new_monitor.entities
			p entities
			#persist it
			save_monitor
		end
	end
end
