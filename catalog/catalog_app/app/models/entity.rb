class Entity < ActiveRecord::Base

	attr_accessible :name

	belongs_to :monitor_node

	has_many :feeds, :dependent => :destroy

	'' '@param data: {"href"=>"www.monitor.com/entities/1/",
       "name"=>"www.myhost.com",
       "feeds"=>
        {"feed"=> ...}}}}' ''

	def self.new_from_hash(data)
		Entity.new.tap do |e|
			e.name = data[:name]
			e.new_feeds_from_hash(data)
		end
	end


	def new_feeds_from_hash(data)
		hash_feeds = data[:feeds][:feed]

		#when there is only one feed it is not in list. we have to fix it
		if (hash_feeds.class != Array)
			hash_feeds = [data[:feeds][:feed]]
		end

		#create feed model from each feed hash representation and add it to entity
		hash_feeds.each do |hash_f|
			feed = Feed.new_from_hash(hash_f)
			self.feeds << feed
		end
	end
end
