class ApplicationController < ActionController::Base
	#protect_from_forgery


	before_filter :authenticate

	before_filter :set_monitoring_content_type
	before_filter :set_base_url

	#around_filter  :error_handling

	def set_monitoring_content_type
		response.headers["Content-Type"] = 'application/xml'
		#response.headers["Content-Type"] = 'text/xml'
	end

	def set_base_url
		@address_base = "#{request.protocol}#{request.host_with_port}"
	end

	def authenticate
		authenticate_or_request_with_http_basic do |username, password|
			if username=="igor" && password=="ala123"
				@user = "igor"
				return true
			end
			if username=="pawel" && password=="ala123"
				@user = "pawel"
				return true
			end
			false
		end
	end

	def current_user
		@user
	end

	def error_handling
		begin
			yield
		rescue Exception => error
		    @error = error
			@title = "Application error"
			@code = 001
			render "error/error"
		end
	end
end