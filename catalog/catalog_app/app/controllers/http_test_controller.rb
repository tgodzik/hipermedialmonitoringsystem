class HttpTestController < ApplicationController

  skip_before_filter :set_monitoring_content_type

  def test
    @test = 'get' if request.get?
    @test = 'post' if request.post?
    @test = 'put' if request.put?
    @test = 'delete' if request.delete?
  end

end
