class FeedsController < ApplicationController

	def index

		if params[:id]
			feed = Feed.find_by_id(params[:id])
			@feeds = []
			@feeds << feed if !feed.nil? && (feed.feed_type == Feed::SIMPLE || feed.access == Feed::PUBLIC || feed.username == current_user)
		else
			metric_name=params[:metric_name]
			metric_unit=params[:metric_unit]
			entity_name=params[:entity_name]
			entity_id = params[:entity_id]


			search = Feed.scoped

			search = search.where("username = ? or access = ? or feed_type = ?", current_user, Feed::PUBLIC, Feed::SIMPLE)

			if (!metric_name.blank?)
				search = search.joins(:metric).where(metrics: {name: metric_name})
			end

			if (!metric_unit.blank?)
				search = search.joins(:metric).where(metrics: {unit: metric_unit})
			end

			if (!entity_name.blank?)
				search = search.joins(:entity).where(entities: {name: entity_name})
			end

			if(entity_id)
				search = search.joins(:entity).where(entities: {id: entity_id})
			end

			@feeds = search
		end

		#respond_to do |format|
		#	format.xml
		#end
	end


end