class ErrorController < ApplicationController

	def error
		@error = Exception.new(404)
		@title = "Page not found"
		@code = 002
		render "error/error"
	end
end