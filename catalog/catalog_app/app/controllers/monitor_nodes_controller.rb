class MonitorNodesController < ApplicationController

	before_filter :authenticate
	require 'pp'


	def index
		@monitors = MonitorNode.all
		p @monitors
		p params
		render :index
	end

	def show
		@monitors = []
		monitor = MonitorNode.find_by_id(params[:id])
		@monitors << monitor if !monitor.nil?

		render :index
	end

	def create
		@monitor = MonitorNode.new_from_hash(params[:data])
		@monitor.save
		debug_print()
		#@monitor.save_monitor

		render :show

	end

	def edit
		#MonitorNode.transaction do

			@monitor_old = MonitorNode.find_by_id(params[:id])
			@monitor_old.destroy

			#@monitor = MonitorNode.find_by_id(params[:id])



			@monitor = MonitorNode.new_from_hash(params[:data])
			@monitor.save

		#end

		#@monitor.update_monitor(params)

		render :show
	end

	private

	def debug_print
		pp params
		@params = params

		pp "MONITOR:"
		pp @monitor
		pp @monitor.entities
		@monitor.entities.each do |e|
			fs = e.feeds
			fs.each do |f|
				pp f
				pp " metric" + f.metric.to_s
			end
		end
	end

end
