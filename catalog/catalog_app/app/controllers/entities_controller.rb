class EntitiesController < ApplicationController

  def index
	  metric_name=params[:metric_name]
	  metric_unit=params[:metric_unit]
	  entity_name=params[:entity_name]

	  search = Entity.scoped


	  if(!metric_name.blank?)
		  search = search.joins(feeds: :metric).where(metrics: {name: metric_name})
	  end

	  if(!metric_unit.blank?)
		  search = search.joins(feeds: :metric).where(metrics: {unit: metric_unit})
	  end

	  if(!entity_name.blank?)
		  search = search.where(name: entity_name)
	  end

	  @entities = search

	  render "show"
  end

  def show
	  @entities = []
	  entity = Entity.find_by_id(params[:id])
	  @entities << entity if !entity.nil?
  end
  
end
