# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121210102632) do

  create_table "entities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "monitor_node_id"
  end

  create_table "feed_parts", :force => true do |t|
    t.integer  "custom_feed_id"
    t.integer  "simple_feed_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "feeds", :force => true do |t|
    t.string   "measurement_url"
    t.integer  "available_measurements", :null => false
    t.integer  "feed_type",              :null => false
    t.integer  "measure_interval"
    t.integer  "avg_measurements_count"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "monitor_node_id"
    t.integer  "entity_id"
    t.integer  "metric_id"
    t.string   "monitor_feed_url"
    t.integer  "avg_period"
    t.string   "username"
    t.integer  "access"
  end

  add_index "feeds", ["monitor_feed_url"], :name => "index_feeds_on_monitor_feed_url", :unique => true

  create_table "metrics", :force => true do |t|
    t.string   "name",       :null => false
    t.string   "unit",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "metrics", ["name", "unit"], :name => "index_metrics_on_name_and_unit", :unique => true

  create_table "monitor_nodes", :force => true do |t|
    t.string   "monitor_url", :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "monitor_nodes", ["monitor_url"], :name => "index_monitor_nodes_on_monitor_url", :unique => true

end
