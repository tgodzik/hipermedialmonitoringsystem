class CreateMonitorNodes < ActiveRecord::Migration
  def change
    create_table :monitor_nodes do |t|
      t.string :monitor_url

      t.timestamps
    end
  end
end
