class AddUsernameToFeeds < ActiveRecord::Migration
  def change
	add_column :feeds, :username, :string
	add_column :feeds, :access, :integer
  end
end
