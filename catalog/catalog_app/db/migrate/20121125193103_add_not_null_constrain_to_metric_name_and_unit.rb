class AddNotNullConstrainToMetricNameAndUnit < ActiveRecord::Migration
  def change
	  change_column :metrics, :name, :string,  null: false
	  change_column :metrics, :unit, :string, null: false
  end
end
