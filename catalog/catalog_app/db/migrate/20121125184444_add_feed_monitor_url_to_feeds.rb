class AddFeedMonitorUrlToFeeds < ActiveRecord::Migration
	def change
		add_column :feeds, :monitor_feed_url, :string
		add_index :feeds, :monitor_feed_url, unique: true
	end
end
