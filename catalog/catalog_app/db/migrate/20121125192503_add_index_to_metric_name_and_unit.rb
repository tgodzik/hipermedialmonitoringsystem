class AddIndexToMetricNameAndUnit < ActiveRecord::Migration
  def change
	  add_index :metrics, [:name, :unit], unique: true
  end
end
