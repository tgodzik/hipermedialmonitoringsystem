class AddNotNullConstraintToMonitorNodeMonitorUrl < ActiveRecord::Migration
  def change
	  change_column :monitor_nodes, :monitor_url, :string, null: false
  end
end
