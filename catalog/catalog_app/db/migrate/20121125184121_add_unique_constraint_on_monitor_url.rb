class AddUniqueConstraintOnMonitorUrl < ActiveRecord::Migration
  def change
		add_index :monitor_nodes, :monitor_url, unique: true
  end


end
