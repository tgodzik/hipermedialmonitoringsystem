class AddMonitorNodeReferenceToFeed < ActiveRecord::Migration
  def change
	  change_table :feeds do |t|
		  t.references :monitor_node
	  end
  end
end
