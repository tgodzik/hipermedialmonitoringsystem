class AddMetricReferenceToFeeds < ActiveRecord::Migration
	change_table :feeds do |t|
		t.references :metric
	end
end
