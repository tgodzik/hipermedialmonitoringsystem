class CreateFeedParts < ActiveRecord::Migration
	def change
		create_table :feed_parts do |t|
	 		t.belongs_to :custom_feed
	 		t.belongs_to :simple_feed
			t.timestamps
		end
	end
end
