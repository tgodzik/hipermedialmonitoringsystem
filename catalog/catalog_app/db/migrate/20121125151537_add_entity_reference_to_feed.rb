class AddEntityReferenceToFeed < ActiveRecord::Migration
  def change
	  change_table :feeds do |t|
		  t.references :entity
	  end
  end
end
