class CreateFeeds < ActiveRecord::Migration
	def change
		create_table :feeds do |t|
			t.string :measurement_url
			t.integer :available_measurements, null: false
			t.integer :feed_type, null: false
			t.integer :measure_interval
			t.integer :avg_measurements_count

			t.timestamps
		end
	end
end
