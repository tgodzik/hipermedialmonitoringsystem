class AddMonitorNodeReferenceToEntities < ActiveRecord::Migration
	def change
		change_table :entities do |t|
			t.references :monitor_node
		end
	end
end
