package rest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import logic.Configuration;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

public class GetRequest {
	private String authorizationValue;
	
	public GetRequest(){
		StringBuilder builder = new StringBuilder();
		builder.append(Configuration.getProperty("login"));
		builder.append(":");
		builder.append(Configuration.getProperty("password"));
		
		String toEncode = builder.toString();
		String encodedString = new String(Base64.encodeBase64(toEncode.getBytes()));

		authorizationValue = "Basic " + encodedString; 
	}
	
	public String get(String uri) {
		try {

			URL url = new URL(uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");
			conn.setRequestProperty("Authorization", authorizationValue);

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			String response = IOUtils.toString(conn.getInputStream(), "UTF-8");
			conn.disconnect();

			return response;

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return "";
	}

}
