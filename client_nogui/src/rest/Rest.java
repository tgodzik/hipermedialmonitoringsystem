package rest;

import org.dom4j.DocumentException;

public class Rest {
	private static GetRequest getRequest = new GetRequest();
	
	public static String getAllMonitors(String catalogAddressUrl) {
		return getRequest.get(catalogAddressUrl+"monitors");
	}
	
	public static String getAllEntities(String monitorUrlAddress) throws DocumentException{
		return getRequest.get(monitorUrlAddress+"/entities");
	}
	
	public static String getAllFeeds(String url, String entityNumber){
		return getRequest.get(url+"entities/"+entityNumber+"/feeds");
	}
	
	public static String getAllFeeds(String baseUrl){
		return getRequest.get(baseUrl+"/feeds");
	}
	
	public static String getAllMeasurements(String url, String feedNumber){
		return getRequest.get(url+"feeds/"+feedNumber+"/measurements");
	}
	
	public static String getAllMeasurements(String baseUrl){
		return getRequest.get(baseUrl+"/measurements");
	}
}
