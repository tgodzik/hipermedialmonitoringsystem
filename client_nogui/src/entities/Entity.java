package entities;

import java.util.HashSet;
import java.util.Set;

import org.dom4j.DocumentException;

import rest.Rest;
import xml.XmlParser;


public class Entity extends BaseEntity implements Runnable{
	private Set<Feed> feeds = new HashSet<>();
	private int feedsRefreshTime = 20000;
	
	public Entity(String entityId, String baseUrl, String urlSufix) {
		super(entityId, baseUrl, baseUrl+urlSufix);
		feedsRefreshTime = getPropertyFromConfiguration("entity.feedsRefreshTime", feedsRefreshTime);
	}
	
	/*
	private String _entityId; 
	private HashSet<Measurement> _feeds;
	
	public String getEntityId(){
		return _entityId;
	}
	
	public Entity(HashSet<Measurement> feeds){
		_feeds = feeds;
	}
	
	public HashSet<Measurement> getFeeds(){
		return _feeds;
	}
	*/
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof Entity)){
			return false;
		}
		
		return super.equals(obj);
	}

	@Override
	public void run() {
		try{
			while(true){
				refreshFeedsList();
				Thread.sleep(feedsRefreshTime);
			}
		} catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	private void refreshFeedsList(){
		try {
			String response = Rest.getAllFeeds(getToEntityUrl());
			HashSet<Feed> set = XmlParser.parseFeed(response, "//monitoring/entities/entity/feeds/feed", getBaseUrl());
			for(Feed feed : set){
				if(feeds.add(feed)){
					new Thread(feed).start();
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
