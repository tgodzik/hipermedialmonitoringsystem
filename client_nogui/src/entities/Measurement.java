package entities;

import java.util.Date;

import logic.ConsoleWriter;

public class Measurement extends BaseEntity{
	private String _value;
	private String _date;
	public Measurement(String entityId, String value, String date, String baseUrl, String urlSufix) {
		super(entityId, baseUrl, baseUrl+urlSufix);
		_value = value;
		_date = date;
	}
	
	public String getValue(){
		return _value;
	}
	
	public String getDate(){
		return _date;
	}

	public void printData(int alarmLevel, String feedType, String feedId) {
		int value = Integer.parseInt(_value);
		if(value >= alarmLevel){
//			System.out.println("Feed number (CPU usage): " + feed.getBaseEntityId() + " measurement id: " + i.getBaseEntityId() + " is equal " + usage);
			ConsoleWriter.println(getPrintableDate() + ": Feed number(" + feedType + "): " + feedId + " measurement id: " + getBaseEntityId() + " is equals " + _value);
		}
	}
	
	private String getPrintableDate(){
		try{
			Date date = new Date(Long.parseLong(_date));
			return date.toString();
		} catch (NumberFormatException e){
			e.printStackTrace();
			return "xx-xx-xxxx";
		}
	}
	
	/*
	private String _measurmentId; 
	
	public String getMeasurementId(){
		return _measurmentId;
	}
	*/
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof Measurement)){
			return false;
		}
		
		Measurement ohter = (Measurement)obj;
		return this._date.equals(ohter._date); 
	}
}
