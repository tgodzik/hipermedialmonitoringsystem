package entities;

import java.util.HashSet;
import java.util.Set;

import org.dom4j.DocumentException;

import rest.Rest;
import xml.XmlParser;

public class Monitor extends BaseEntity implements Runnable {
	private Set<Entity> entities = new HashSet<>();
	private int entitiesRefreshTime = 30000;
	private static final String TO_TEST_URL = "http://monitor.codewatch.pl/";

	public Monitor(String entityId, String baseUrl) {
		super(entityId, baseUrl, baseUrl);
//		super(entityId, TO_TEST_URL, TO_TEST_URL);
		entitiesRefreshTime = getPropertyFromConfiguration(
				"monitor.entitesRefreshTime", entitiesRefreshTime);
	}

	@Override
	public void run() {
		try {
			while (true) {
				refreshEntitesList();
				Thread.sleep(entitiesRefreshTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void refreshEntitesList() {
		try {
			String response = Rest.getAllEntities(getToEntityUrl());
			HashSet<Entity> set = XmlParser.parseEntities(response, "//entities/entity", getBaseUrl());
			for (Entity entity : set) {
				if (entities.add(entity)) {
					new Thread(entity).start();
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/*
	 * private String _urlAddress; private HashSet<Entity> _entities;
	 * 
	 * public Monitor(HashSet<Entity> entities){ _entities = entities; }
	 * 
	 * public String getUrlAddress(){ return _urlAddress; }
	 * 
	 * public HashSet<Entity> getEntities(){ return _entities; }
	 */

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Monitor)) {
			return false;
		}
		return super.equals(obj);
	}
}
