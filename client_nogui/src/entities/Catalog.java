package entities;

import java.util.HashSet;
import java.util.Set;

import org.dom4j.DocumentException;

import rest.Rest;
import xml.XmlParser;

public class Catalog extends BaseEntity implements Runnable {
	private Set<Monitor> monitors = new HashSet<>();
	private int monitorRefreshTime = 60000;

	public Catalog(String entityId, String urlToElement) {
		super(entityId, urlToElement, urlToElement);
		monitorRefreshTime = getPropertyFromConfiguration("catalog.monitorRefreshTime", monitorRefreshTime);
	}

	@Override
	public void run() {
		try {
			while (true) {
				refreshMonitorsList();
				Thread.sleep(monitorRefreshTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void refreshMonitorsList() {
		try {
			String string = Rest.getAllMonitors(getBaseUrl());
			HashSet<Monitor> set = XmlParser.parseMonitors(string, "//monitoring/monitors/monitor");
			for (Monitor monitor : set) {
				if(monitors.add(monitor)){
					new Thread(monitor).start();
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
