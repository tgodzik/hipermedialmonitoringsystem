package entities;

import java.util.HashSet;
import java.util.Set;

import logic.Configuration;

import org.dom4j.DocumentException;

import rest.Rest;
import xml.XmlParser;

public class Feed extends BaseEntity implements Runnable{
	public String _feedType;
	private int measurmentsRefreshTime = 5000;
	private Set<Measurement> measurments = new HashSet<>();
	private boolean isObservable;
	private int alarmLevel;
	
	public Feed(String baseEntityId, String feedType, String baseUrl, String urlSufix) {
		super(baseEntityId, baseUrl, baseUrl+urlSufix);
		_feedType = feedType;
		measurmentsRefreshTime = getPropertyFromConfiguration("feed.measurmentsRefreshTime", measurmentsRefreshTime);
		isObservable = Configuration.isObservable(_feedType);
		if(isObservable){
			alarmLevel = Configuration.getAlarmLevelForFeed(_feedType);
		}
	}
	
	public String getFeedType(){
		return _feedType;
	}

	@Override
	public void run() {
		try{
			while(isObservable){
				refreshMeasurments();
				Thread.sleep(measurmentsRefreshTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	private void refreshMeasurments(){
		try {
			String response = Rest.getAllMeasurements(getToEntityUrl());
			HashSet<Measurement> set = XmlParser.parseMeasurement(response, "//monitoring/feeds/feed/measurements/measurement", getBaseUrl());
			for(Measurement measurement : set){
				if(measurments.add(measurement)){
					measurement.printData(alarmLevel, _feedType, getBaseEntityId());
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof Feed)){
			return false;
		}
		return super.equals(obj);
	}
	
	/*
	private String _feedId; 
	private HashSet<Measurement> _measurements;
	
	public String getFeedId(){
		return _feedId;
	}
	
	public Feed(HashSet<Measurement> measurements){
		_measurements = measurements;
	}
	
	public HashSet<Measurement> getMeasurements(){
		return _measurements;
	}
	*/
}
