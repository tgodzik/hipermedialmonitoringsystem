package entities;

import java.util.HashSet;

import logic.Configuration;

public abstract class BaseEntity {
	private String _baseEntityId;
	private String baseUrl;
	private String toEntityUrl;
	public String getToEntityUrl() {
		return toEntityUrl;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	private HashSet<BaseEntity> _entitiesSet = new HashSet<BaseEntity>();
	
	public BaseEntity(String entityId, String baseUrl, String toEntityUrl){
		_baseEntityId = entityId;
		this.baseUrl = baseUrl;
		this.toEntityUrl = toEntityUrl;
	}
	
	public void addEntities(HashSet<BaseEntity> entities){
		_entitiesSet.addAll(entities);
	}
	
	public void addEntity(BaseEntity entity){
		_entitiesSet.add(entity);
	}
	
	public HashSet<BaseEntity> getEntities(){
		return _entitiesSet;
	}
	
	public String getBaseEntityId(){
		return _baseEntityId;
	}
	
	protected int getPropertyFromConfiguration(String property, int defaultValue){
		try{
			int value = Integer.parseInt(Configuration.getProperty(property));
			return value;
		} catch (NumberFormatException e){
			e.printStackTrace();
			return defaultValue;
		}
	}
	
	@Override 
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof BaseEntity)){ 
			return false;
		}
		
		BaseEntity other = (BaseEntity)obj;
		return this._baseEntityId.equals(other._baseEntityId);
	}
	
	@Override
	public int hashCode(){
		int hashCode = addToHash(0, _baseEntityId);
		hashCode = addToHash(hashCode, baseUrl);
		hashCode = addToHash(hashCode, toEntityUrl);
		return hashCode;
	}
	
	private int addToHash(int hash, String str){
		return (hash/2)+(str.hashCode()/2);
	}
}
