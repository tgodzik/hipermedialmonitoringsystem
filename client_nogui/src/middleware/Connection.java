package middleware;


public class Connection {
	private static Connection _instance = null;
	
	private Connection(){
		
	}
	
	public static Connection getInstance(){
		if (_instance == null){
			_instance = new Connection();
		}
		return _instance;
	}
	
	/*
	
	public static List<String> getAllMonitors() throws DocumentException{
		List<String> list = XmlParser.parse(Rest.getAllMonitors());
		return null;
		//return (Xml.parseAllMonitors(rest.getAllMonitors()));
	}
	*/
	
	/*
	public List<String> getAllEntities(String monitorUrlAddress) throws DocumentException{
		List<String> list = XmlParser.parse(Rest.getAllEntities(monitorUrlAddress));
		return Rest.getAllEntities(monitorUrlAddress);
	}
	
	public HashSet<Feed> getFeeds(int entityNumber){
		return Rest.getInstance().getFeeds(entityNumber);
	}
	
	public HashSet<Measurement> getMeasurements(int entityNumber, int feedNumber){
		return Rest.getInstance().getMeasurements(entityNumber, feedNumber);
	}
	*/
}
