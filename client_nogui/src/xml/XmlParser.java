package xml;

import java.util.HashSet;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;

import entities.Entity;
import entities.Feed;
import entities.Measurement;
import entities.Monitor;

public class XmlParser {
	public static HashSet<String> parse(String string, String xPath) throws DocumentException{
        Document document = DocumentHelper.parseText(string);
        
        //System.out.print(document.asXML());
        //System.out.println();
        HashSet<String> ret = new HashSet<String>();
        
        
        List<Node> nodes = document.selectNodes(xPath);
        for (Node node : nodes){
        	String nodeValue = node.valueOf("@id");
			//System.out.println(nodeValue );
			ret.add(nodeValue);
		}
        
		return ret;
	}
	
	public static HashSet<Entity> parseEntities(String string, String xPath, String baseUrl) throws DocumentException{
		Document document = DocumentHelper.parseText(string);
		HashSet<Entity> entities = new HashSet<>();
		
		List<Node> nodes = document.selectNodes(xPath);
		for(Node node : nodes){
			entities.add(new Entity(node.valueOf("@id"), baseUrl, node.valueOf("@href")));
		}
		
		return entities;
	}
	
	public static HashSet<Monitor> parseMonitors(String string, String xPath) throws DocumentException{
		Document document = DocumentHelper.parseText(string);
		HashSet<Monitor> monitors = new HashSet<Monitor>();
		
		List<Node> nodes = document.selectNodes(xPath);
		for(Node node : nodes){
			monitors.add(new Monitor(node.valueOf("@id"), node.valueOf("monitor_url")));
		}
		
		return monitors;
	}
	
	public static HashSet<Measurement> parseMeasurement(String string, String xPath, String baseUrl) throws DocumentException{
        Document document = DocumentHelper.parseText(string);
        HashSet<Measurement> ret = new HashSet<Measurement>();
        
        List<Node> nodes = document.selectNodes(xPath);
        for (Node node : nodes){
			ret.add(new Measurement(node.valueOf("@id"), node.valueOf("value"), node.valueOf("date"), baseUrl, node.valueOf("@href")));
		}
        
		return ret;
	}
	
	public static HashSet<Feed> parseFeed(String string, String xPath, String baseUrl) throws DocumentException{
        Document document = DocumentHelper.parseText(string);
        HashSet<Feed> ret = new HashSet<Feed>();
        
        List<Node> nodes = document.selectNodes(xPath);
        for (Node node : nodes){
			ret.add(new Feed(node.valueOf("@id"), node.valueOf("metric/name"), baseUrl, node.valueOf("@href")));
			//System.out.println(node.valueOf("metric/name"));
        }
        
		return ret;
	}
}
