package logic;

import java.util.Arrays;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Configuration {
	private static final String BUNDLE_NAME = Configuration.class.getName().toLowerCase();
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	private static int defaultAlarmLevel = 50;

	public static String getProperty(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private static String[] getParamsArray(String paramName){
		String observableFeedsStr = getProperty(paramName);
		return observableFeedsStr.split(";");
	}
	
	public static boolean isObservable(String feedName){
		return Arrays.asList(getParamsArray("observablesFeeds")).contains(feedName);
	}
	
	public static int getAlarmLevelForFeed(String feedName){
		String[] observablesFeeds = getParamsArray("observablesFeeds");
		String[] feedsAlarmsLevels = getParamsArray("observablesFeedsAlarmLevels");
		
		int feedNumber;
		for(feedNumber = 0; feedNumber < observablesFeeds.length; feedNumber++){
			if(observablesFeeds[feedNumber].equals(feedName) && feedNumber < feedsAlarmsLevels.length){
				try{
					int alaramLevel = Integer.parseInt(feedsAlarmsLevels[feedNumber]);
					return alaramLevel;
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		
		return defaultAlarmLevel;
	}

}