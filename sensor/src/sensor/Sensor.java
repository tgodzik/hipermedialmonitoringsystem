package sensor;

public interface Sensor {

	public String getName();

	public String getUnit();

	public int getData();

}
