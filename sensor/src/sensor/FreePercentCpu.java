package sensor;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

public class FreePercentCpu implements Sensor {

	private static Sigar sigar = new Sigar();

	@Override
	public String getName() {
		return "Free percentage cpu";
	}

	@Override
	public String getUnit() {
		return "%";
	}

	@Override
	public int getData() {
		CpuPerc cpuPerc = null;
		try {
			cpuPerc = sigar.getCpuPerc();
		} catch (SigarException se) {
			se.printStackTrace();
		}
		System.out.println(cpuPerc.getIdle());

		return (int) (cpuPerc.getIdle() * 100);
	}

}
