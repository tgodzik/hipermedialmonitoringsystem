package sensor;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;


public class UsedPercentMem implements Sensor {

	private static Sigar sigar = new Sigar();

	@Override
	public String getName() {
		return "Percent total used mem";
	}

	@Override
	public String getUnit() {
		return "%";
	}

	@Override
	public int getData() {
		Mem mem = null;
		try {
			mem = sigar.getMem();
		} catch (SigarException se) {
			se.printStackTrace();
		}

		return (int) (mem.getUsedPercent());
	}
}
