package runner;

import manager.SensorManager;
import manager.SimpleSensorManager;
import connector.ConnectionProperties;

public class Runner {

	public static void main(String args[]) {

		if (args.length != 3) {
			throw new IllegalArgumentException("Arguments: <entity_name> <host> <port>");
		}

		String host = args[1];
		String name = args[0];
		int port = Integer.parseInt(args[2]);
		ConnectionProperties connectionProperties = new ConnectionProperties(name,host, port);

		SensorManager sensorManager = new SimpleSensorManager(connectionProperties);
		sensorManager.registerSensorsInMonitor();
		sensorManager.sendDataToMonitor();
	}

}
