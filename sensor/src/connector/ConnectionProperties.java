package connector;

public class ConnectionProperties {

	private String host,name;
	private int port;

	public ConnectionProperties(String name,String host, int port) {
		this.host = host;
		this.port = port;
		this.name=name;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}
	
	public String getName() {
		return name;
	}

}
