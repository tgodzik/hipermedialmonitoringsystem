package connector;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import sensor.Sensor;

public class MonitorConnector {

	private Sensor sensor;
	private Socket socket;
	private ConnectionProperties connectionProperties;

	private BufferedWriter out;

	public MonitorConnector(ConnectionProperties connectionProperties) {
		this.connectionProperties = connectionProperties;
	}

	public void registerSensor(Sensor sensor) {
		this.sensor = sensor;
		createConnectionAndGetDataOutputStream();

		try {
			out.write("REGISTER:ENTITY:"+this.connectionProperties.getName() +":METRIC:"+sensor.getName()+":UNIT:"+sensor.getUnit()+":\n");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendDataFromSensor() {
		try {
			periodicallySendData();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void createConnectionAndGetDataOutputStream() {
		try {
			String host = connectionProperties.getHost();
			int port = connectionProperties.getPort();

			socket = new Socket(host, port);
			out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		   
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void periodicallySendData() throws InterruptedException, IOException {
		
			
			out.write("DATA:"+sensor.getData()+":\n");
			out.flush();
		
	}
	
	public void closeConnection(){
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
