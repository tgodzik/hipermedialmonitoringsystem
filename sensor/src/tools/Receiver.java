package tools;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

// Listens only on 9001, can test only 1 socket
public class Receiver {

	public static void main(String args[]) throws IOException {
		ServerSocket serverSocket = new ServerSocket(9001);
		try {
			while (true) {
				Socket socket = serverSocket.accept();
				DataInputStream in = new DataInputStream(socket.getInputStream());

				int nameLength = in.readInt();
				byte[] name = new byte[nameLength];
				in.read(name);
				System.out.println(new String(name));

				int unitLength = in.readInt();
				byte[] unit = new byte[unitLength];
				in.read(unit);
				System.out.println(new String(unit));

				// print data
				while (true) {
					System.out.println(in.readInt());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			serverSocket.close();
		}

	}

}
