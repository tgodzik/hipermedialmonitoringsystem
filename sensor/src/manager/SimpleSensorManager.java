package manager;

import java.util.ArrayList;
import java.util.List;

import sensor.Sensor;
import connector.ConnectionProperties;
import connector.MonitorConnector;

public class SimpleSensorManager implements SensorManager {

	private List<Sensor> allSensors;

	private List<MonitorConnector> allMonitorConnectors = new ArrayList<>();

	private ConnectionProperties connectionProperties;

	public SimpleSensorManager(ConnectionProperties connectionProperties) {
		this.connectionProperties = connectionProperties;
		allSensors = SensorScanner.getSensors();
	}

	@Override
	public void registerSensorsInMonitor() {
		for (Sensor sensor : allSensors) {
			MonitorConnector monitorConnector = new MonitorConnector(connectionProperties);
			monitorConnector.registerSensor(sensor);

			allMonitorConnectors.add(monitorConnector);
		}
	}

	@Override
	public void sendDataToMonitor() {
		while(true){
			for (MonitorConnector connector : allMonitorConnectors) {
				connector.sendDataFromSensor();
			}

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
