package manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import sensor.Sensor;

public class SensorScanner {

	public static List<Sensor> getSensors() {
		List<Sensor> sensors = new ArrayList<>();

		for (Class<? extends Sensor> sensor : parseSensorPackage()) {
			sensors.add(castSensor(sensor));
		}

		return sensors;
	}

	private static Set<Class<? extends Sensor>> parseSensorPackage() {
		Reflections reflections = new Reflections("sensor");
		Set<Class<? extends Sensor>> sensors = reflections.getSubTypesOf(Sensor.class);
		return sensors;
	}

	private static Sensor castSensor(Class<? extends Sensor> sensor) {
		Sensor castedSensor = null;
		try {
			castedSensor = (Sensor) Class.forName(sensor.getName()).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return castedSensor;
	}

}
