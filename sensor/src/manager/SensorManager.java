package manager;

public interface SensorManager {

	public void registerSensorsInMonitor();

	public void sendDataToMonitor();

}
